require 'roda'

require_relative './db_mappings'
require_relative './components/command_runner'

class App < Roda
  route do |r|
    r.root do
      "render static website"
    end

    r.on "api" do
      r.is do
        "render API documentation"
      end

      r.on "command" do
        r.run CommandRunner
      end
    end
  end
end
