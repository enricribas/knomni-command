class Action
  include Hanami::Entity
  attributes :type
  attributes :payload
end
