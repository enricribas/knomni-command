class EventStream
  def self.create(params)
    payload = params.reject {|k,v| k == 'type'}.to_json
    event = Action.new type: params["type"], payload: payload

    ActionRepo.create event
  end
end
