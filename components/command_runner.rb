require 'bundler/setup'
require 'pry'
require 'roda'

require_relative '../lib/event_stream'

require_relative '../commands/stub_command'
require_relative '../commands/tag_website_command'
require_relative '../commands/set_url_command'
require_relative '../commands/search_tags_command'

COMMANDS = {
  'ADD_TODO'    => TagWebsiteCommand,
  'SET_URL'     => SetUrlCommand,
  'SEARCH_TAGS' => SearchTagsCommand
}

class CommandRunner < Roda
  plugin :json
  plugin :json_parser # seems to not happen
  plugin :default_headers, 'Access-Control-Allow-Origin' => '*'

  route do |r|
    r.on ":command", method: :options do
      {}
    end

    r.post ":command" do |command|
      params = JSON.parse r.body.read
      EventStream.create(params)

      cmd = COMMANDS.fetch(command, StubCommand).new params

      # TODO rescue errors and return JSON
      cmd.validate!
      cmd.run
    end
  end
end
