require 'hanami/model'

require_relative './models/action'
require_relative './repos/action_repo'

Hanami::Model.configure do
  adapter type: :sql, uri: 'postgres://localhost/knomni_development'

  mapping do
    collection :actions do
      entity     Action
      repository ActionRepo

      attribute :type,    String
      attribute :payload, String
    end
  end
end

Hanami::Model.load!
