class Commands
  Error           = Class.new(StandardError)
  ValidationError = Class.new(Error)

  def initialize(params)
    @params = params
  end

  def validate!
    create_record!
    raise ValidationError unless valid?
  end

  def valid?
    true
  end

  def run
    {}
  end

  protected

  def params
    @params || {}
  end

  def create_record!
    
  end
end
