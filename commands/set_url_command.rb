require_relative './commands'

class SetUrlCommand < Commands
  def run
    {
      url: "http://www.msn.com",
      tags: ['portal', 'news', 'gossip']
    }
  end

  def valid?
    true
  end
end
