require_relative './commands'

class SearchTagsCommand < Commands
  def run
    {
      urls: [
        {
          url: "http://www.msn.com",
          tags: ['portal', 'news', 'gossip']
        }
      ]
    }
  end

  def valid?
    true
  end
end
